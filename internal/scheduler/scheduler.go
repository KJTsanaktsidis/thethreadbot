package scheduler

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	badger "github.com/dgraph-io/badger/v3"
	"github.com/sirupsen/logrus"
	"github.com/thanhpk/randstr"

	"gitlab.com/KJTsanaktsidis/thethreadbot/internal/config"
)

func NewScheduler(ctx context.Context, c config.Config, cl *http.Client, logger logrus.FieldLogger) (*Scheduler, error) {
	db, err := badger.Open(badger.DefaultOptions(c.DataDirectory))
	if err != nil {
		return nil, fmt.Errorf("failed to open database: %w", err)
	}

	sched := &Scheduler{
		c:      c,
		db:     db,
		nextC:  make(chan struct{}, 1),
		logger: logger,
		cl:     cl,
	}

	return sched, nil
}

type Scheduler struct {
	db     *badger.DB
	c      config.Config
	nextC  chan struct{}
	logger logrus.FieldLogger
	cl     *http.Client
}

func (s *Scheduler) Run(ctx context.Context) error {
	var timer *time.Timer
	defer func() {
		if timer != nil {
			timer.Stop()
		}
	}()

	for {
		if timer != nil {
			timer.Stop()
			timer = nil
		}

		nextJob, err := s.getNext(ctx)
		if err != nil {
			return err
		}
		var timerC <-chan time.Time
		if nextJob != nil {
			timer = time.NewTimer(time.Until(nextJob.PingAt))
			timerC = timer.C
			s.logger.Infof("Next job at %s", nextJob.PingAt.Format(time.RFC3339))
		} else {
			s.logger.Infof("No next job present. Sleeping forever.")
		}

		select {
		case <-ctx.Done():
			return nil
		case <-timerC: // will block forever if no nextJob present
			s.logger.Infof("Processing job %+v", *nextJob)
			if err := s.handleJob(ctx, *nextJob); err != nil {
				s.logger.WithError(err).Errorf("Scheduler failed")
				return fmt.Errorf("failed processing job: %w", err)
			}
		case <-s.nextC:
			// someone sent us a job.
			s.logger.Infof("Received job; relooping.")
		}
	}
}

func (s *Scheduler) Close() error {
	s.db.Close()
	close(s.nextC)
	return nil
}

func (s *Scheduler) getNext(ctx context.Context) (*PingThreadJob, error) {
	var retJob *PingThreadJob
	err := s.db.View(func(txn *badger.Txn) error {
		it := txn.NewIterator(badger.DefaultIteratorOptions)
		defer it.Close()
		prefix := []byte("PingThreadJob/")
		it.Seek(prefix)
		if !it.ValidForPrefix(prefix) {
			return nil
		}
		item := it.Item()
		return item.Value(func(val []byte) error {
			err := json.Unmarshal(val, &retJob)
			if err != nil {
				return fmt.Errorf("failed unmarshalling job %s: %w", string(item.Key()), err)
			}
			return nil
		})
	})
	if err != nil {
		return nil, fmt.Errorf("failed fetching job from db: %w", err)
	}
	return retJob, nil
}

func (s *Scheduler) ScheduleJob(job PingThreadJob) error {
	job.JobID = randstr.Hex(12)
	if err := s.enqueueJob(job); err != nil {
		return nil
	}
	// wake up the loop
	select {
	case s.nextC <- struct{}{}:
	default:
	}
	return nil
}

func (s *Scheduler) enqueueJob(job PingThreadJob) error {
	err := s.db.Update(func(txn *badger.Txn) error {
		key := fmt.Sprintf("PingThreadJob/%024d/%s", job.PingAt.Unix(), job.JobID)
		value, err := json.Marshal(job)
		if err != nil {
			return fmt.Errorf("failed marshaling job: %w", err)
		}
		if err := txn.Set([]byte(key), value); err != nil {
			return fmt.Errorf("failed saving job %s: %w", key, err)
		}
		return nil
	})
	if err != nil {
		return fmt.Errorf("failed saving job txn: %w", err)
	}
	return nil
}

func (s *Scheduler) handleJob(ctx context.Context, job PingThreadJob) error {
	err := s.pingThread(ctx, job)
	if err != nil {
		return fmt.Errorf("failed pinging thread: %w", err)
	}

	// Now schedule a copy of the job for the future
	err = s.db.Update(func(txn *badger.Txn) error {
		newJob := job
		newJob.PingAt = time.Now().Add(s.c.PingInterval)
		newJob.JobID = randstr.Hex(12)

		err := txn.Delete([]byte(
			fmt.Sprintf("PingThreadJob/%024d/%s", job.PingAt.Unix(), job.JobID)),
		)
		if err != nil {
			return fmt.Errorf("failed to delete old value: %w", err)
		}

		key := fmt.Sprintf("PingThreadJob/%024d/%s", newJob.PingAt.Unix(), newJob.JobID)
		value, err := json.Marshal(newJob)
		if err != nil {
			return fmt.Errorf("failed marshaling job: %w", err)
		}
		if err := txn.Set([]byte(key), value); err != nil {
			return fmt.Errorf("failed saving job %s: %w", key, err)
		}
		return nil
	})
	if err != nil {
		return fmt.Errorf("failed reschedule txn: %w", err)
	}
	return nil
}
