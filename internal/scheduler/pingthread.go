package scheduler

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/KJTsanaktsidis/thethreadbot/internal/discord"
)

type PingThreadJob struct {
	GuildID  string    `json:"guild"`
	ThreadID string    `json:"thread_id"`
	PingAt   time.Time `json:"ping_at"`
	JobID    string    `json:"job_id"`
}

func (s *Scheduler) pingThread(ctx context.Context, job PingThreadJob) error {
	reqBody := discord.PostMessageRequestBody{
		Content: "looollool I am keeping the thread alive!",
	}
	reqBodyBytes, err := json.Marshal(reqBody)
	if err != nil {
		return fmt.Errorf("failed marshaling request body: %w", err)
	}

	req, err := http.NewRequestWithContext(
		ctx,
		http.MethodPost,
		fmt.Sprintf("%s/channels/%s/messages", s.c.DiscordAPIEndpoint, job.ThreadID),
		bytes.NewBuffer(reqBodyBytes),
	)
	if err != nil {
		return fmt.Errorf("failed constructing post message request: %w", err)
	}
	req.Header.Add("Content-type", "application/json")
	req.Header.Add("Authorization", fmt.Sprintf("Bot %s", s.c.BotAuthToken))

	res, err := s.cl.Do(req)
	if err != nil {
		return fmt.Errorf("failed doing HTTP request to post message: %w", err)
	}
	defer res.Body.Close()
	if res.StatusCode < 200 || res.StatusCode >= 300 {
		return fmt.Errorf("failed doing HTTP rquest to post message: http %s", res.Status)
	}

	return nil
}
