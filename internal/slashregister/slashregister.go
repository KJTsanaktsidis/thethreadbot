package slashregister

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/KJTsanaktsidis/thethreadbot/internal/config"
	"gitlab.com/KJTsanaktsidis/thethreadbot/internal/discord"
)

func RegisterSlashCommands(ctx context.Context, c config.Config, cl *http.Client, logger logrus.FieldLogger) error {
	command := discord.CommandRegisterRequestBody{
		Name:        "keepalive",
		Description: "Keeps the thread alive by pinging pointless garbage in here every 24 hours",
	}

	// Register on the named guilds
	for _, guild := range c.DirectSlashRegisterGuilds {
		if err := registerCommand(ctx, c, cl, logger, command, guild); err != nil {
			return err
		}
	}

	if c.EnableGlobalSlashRegister {
		if err := registerCommand(ctx, c, cl, logger, command, ""); err != nil {
			return err
		}
	}

	return nil
}

func registerCommand(ctx context.Context, c config.Config, cl *http.Client, logger logrus.FieldLogger, command discord.CommandRegisterRequestBody, guild string) error {
	var commandUrl string
	var globalOrGuildMsg string
	if guild == "" {
		commandUrl = fmt.Sprintf("%s/applications/%s/commands", c.DiscordAPIEndpoint, c.ApplicationID)
		globalOrGuildMsg = "globally"
	} else {
		commandUrl = fmt.Sprintf("%s/applications/%s/guilds/%s/commands", c.DiscordAPIEndpoint, c.ApplicationID, guild)
		globalOrGuildMsg = fmt.Sprintf("for guild %s", guild)
	}

	commandJson, err := json.Marshal(command)
	if err != nil {
		return fmt.Errorf("failed marshalling command json: %w", err)
	}

	req, err := http.NewRequestWithContext(
		ctx,
		http.MethodPost,
		commandUrl,
		bytes.NewBuffer(commandJson),
	)
	if err != nil {
		return fmt.Errorf("failed making slash-register request %s: %w", globalOrGuildMsg, err)
	}
	req.Header.Add("Authorization", fmt.Sprintf("Bot %s", c.BotAuthToken))
	req.Header.Add("Content-type", "application/json")
	res, err := cl.Do(req)
	if err != nil {
		return fmt.Errorf("failed registering slash-command %s: %w", globalOrGuildMsg, err)
	}
	body, err := io.ReadAll(res.Body)
	if err != nil {
		return fmt.Errorf("failed reading response body: %w", err)
	}
	if res.StatusCode < 200 || res.StatusCode >= 300 {
		logger.WithField("body", string(body)).Errorf("Failed registering command %s: http %d", globalOrGuildMsg, res.Status)
		return fmt.Errorf("failed registering slash-command %s: http %d", globalOrGuildMsg, res.StatusCode)
	}
	logger.Infof("Registered command %s %s: %s", command.Name, globalOrGuildMsg, res.Status)
	return nil
}
