package discord

type CommandRegisterRequestBody struct {
	Name        string                              `json:"name"`
	Description string                              `json:"description"`
	Options     []CommandRegisterRequestBodyOptions `json:"options"`
}

type CommandRegisterRequestBodyOptions struct {
	Name        string                             `json:"name"`
	Description string                             `json:"description"`
	Type        int                                `json:"type"`
	Required    bool                               `json:"required"`
	Choices     []CommandRegisterRequestBodyChoice `json:"choices,omitempty"`
}

type CommandRegisterRequestBodyChoice struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

type IncomingInteractionWebhookRequestBody struct {
	ApplicationID string                          `json:"application_id"`
	ChannelID     string                          `json:"channel_id"`
	GuildID       string                          `json:"guild_id"`
	Id            string                          `json:"id"`
	Token         string                          `json:"token"`
	Type          int                             `json:"type"`
	User          *IncomingInteractionWebhookUser `json:"user,omitempty"`
	Version       int                             `json:"version"`
	// I believe the incoming webhook is polymorphic in data
	Data             interface{}                                 `json:"data,omitempty"`
	SlashCommandData *IncomingInteractionWebhookSlashCommandData `json:"-"`
}

type IncomingInteractionWebhookSlashCommandData struct {
	Id string `json:"id"`
}

type IncomingInteractionWebhookUser struct {
	Id            string `json:"id"`
	Username      string `json:"username"`
	Discriminator string `json:"discriminator"`
}

type IncomingInteractionWebhookResponseBody struct {
	Type int         `json:"type"`
	Data interface{} `json:"data,omitempty"`
}

type IncomingInteractionResponseData struct {
	Content string `json:"content"`
	Flags   int    `json:"flags"`
	// There are plenty of other cool things you can send but this will do for now.
}

type PostMessageRequestBody struct {
	Content string `json:"content"`
	Flags   int    `json:"flags"`
	// Likewise; plenty of options here I will ignore.
}
