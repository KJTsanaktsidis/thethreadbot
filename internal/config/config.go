package config

import "time"

type Config struct {
	ApplicationID             string
	ApplicationPublicKey      string
	OAuth2ClientID            string
	BotAuthToken              string
	DirectSlashRegisterGuilds []string
	EnableGlobalSlashRegister bool
	DiscordAPIEndpoint        string
	HttpListenAddr            string
	HttpListenPort            int
	DataDirectory             string
	PingInterval              time.Duration
}
