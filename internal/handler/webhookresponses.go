package handler

import (
	"context"
	"fmt"
	"time"

	"github.com/mitchellh/mapstructure"
	"gitlab.com/KJTsanaktsidis/thethreadbot/internal/discord"
	"gitlab.com/KJTsanaktsidis/thethreadbot/internal/scheduler"
)

func (h *Handler) dispatchIncomingWebhook(
	ctx context.Context,
	reqBody discord.IncomingInteractionWebhookRequestBody,
) (discord.IncomingInteractionWebhookResponseBody, error) {
	switch reqBody.Type {
	case 1: // ping
		return h.handleIncomingWebhookPing(ctx, reqBody)
	case 2: // seems to be what we get for a slash-command, although it's not super clear
		return h.handleIncomingWebhookSlashCommand(ctx, reqBody)
	default:
		return discord.IncomingInteractionWebhookResponseBody{}, fmt.Errorf("unknown type %d", reqBody.Type)
	}
}

func (h *Handler) handleIncomingWebhookPing(
	ctx context.Context,
	reqBody discord.IncomingInteractionWebhookRequestBody,
) (discord.IncomingInteractionWebhookResponseBody, error) {
	return discord.IncomingInteractionWebhookResponseBody{
		Type: 1,
	}, nil
}

func (h *Handler) handleIncomingWebhookSlashCommand(
	ctx context.Context,
	reqBody discord.IncomingInteractionWebhookRequestBody,
) (discord.IncomingInteractionWebhookResponseBody, error) {

	// Decode the data as a slash-command body
	var res discord.IncomingInteractionWebhookResponseBody
	var structuredData discord.IncomingInteractionWebhookSlashCommandData
	err := mapstructure.Decode(reqBody.Data, &structuredData)
	if err != nil {
		return res, fmt.Errorf("failed parsing .data: %w", err)
	}
	reqBody.SlashCommandData = &structuredData

	// TODO: Actually save the state to come back here in 24 hours.
	job := scheduler.PingThreadJob{
		GuildID:  reqBody.GuildID,
		ThreadID: reqBody.ChannelID,
		PingAt:   time.Now().Add(h.c.PingInterval),
	}
	err = h.sched.ScheduleJob(job)
	if err != nil {
		return res, fmt.Errorf("failed scheduling job: %w", err)
	}

	// And let the user know we saved it.
	res.Type = 4 // non-deferred response
	res.Data = &discord.IncomingInteractionResponseData{
		Content: fmt.Sprintf(
			"OK! I'll keep this thread alive by pinging it every %s",
			h.c.PingInterval.String(),
		),
	}
	return res, nil
}
