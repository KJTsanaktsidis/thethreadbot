package handler

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/KJTsanaktsidis/thethreadbot/internal/config"
	"gitlab.com/KJTsanaktsidis/thethreadbot/internal/discord"
	"gitlab.com/KJTsanaktsidis/thethreadbot/internal/scheduler"
)

func RunHTTPServer(
	ctx context.Context, c config.Config, logger logrus.FieldLogger, scheduler *scheduler.Scheduler,
) error {
	pubkeyBytes, err := decodePublicKey(c.ApplicationPublicKey)
	if err != nil {
		return err
	}
	handler := &Handler{c: c, logger: logger, pubkey: pubkeyBytes, sched: scheduler}

	r := mux.NewRouter()
	r.HandleFunc("/webhook", handler.HandleIncomingWebhook).Methods(http.MethodPost)

	server := http.Server{
		Addr:         fmt.Sprintf("%s:%d", c.HttpListenAddr, c.HttpListenPort),
		Handler:      r,
		ReadTimeout:  15 * time.Second,
		WriteTimeout: 15 * time.Second,
	}

	errChan := make(chan error)
	defer close(errChan)
	go func() {
		logger.Infof("Listening on %s", server.Addr)
		err := server.ListenAndServe()
		if err != nil && !errors.Is(err, http.ErrServerClosed) {
			errChan <- err
		} else {
			errChan <- nil
		}
	}()

	select {
	case <-ctx.Done():
		server.Shutdown(ctx)
		return <-errChan
	case err := <-errChan:
		return err
	}
}

type Handler struct {
	c      config.Config
	logger logrus.FieldLogger
	pubkey [32]byte
	sched  *scheduler.Scheduler
}

func (h *Handler) HandleIncomingWebhook(res http.ResponseWriter, req *http.Request) {
	reqBodyBytes, err := io.ReadAll(req.Body)
	if err != nil {
		h.logger.Errorf("HandleIncomingWebhook failed reading request: %s", err)
		res.WriteHeader(500)
		return
	}

	h.logger.WithField("body", string(reqBodyBytes)).Debugf("HandleIncomingWebhook: %s %s", req.Method, req.URL.Path)

	// Check sig
	sigValid := h.validateWebhookSignature(req.Header, reqBodyBytes)
	if !sigValid {
		h.logger.Debugf("HandleIncomingWebhook: Signature is not valid")
		res.WriteHeader(401)
		return
	} else {
		h.logger.Debugf("HandleIncomingWebhook: Signature is valid")
	}

	var reqBody discord.IncomingInteractionWebhookRequestBody
	err = json.Unmarshal(reqBodyBytes, &reqBody)
	if err != nil {
		h.logger.Errorf("HandleIncomingWebhook: unknown body format: %s", err)
		res.WriteHeader(400)
		return
	}

	resBody, err := h.dispatchIncomingWebhook(req.Context(), reqBody)
	if err != nil {
		h.logger.Errorf("HandleIncomingWebhook: %s", err)
		res.WriteHeader(500)
		return
	}

	resBodyBytes, err := json.Marshal(resBody)
	if err != nil {
		h.logger.Error("HandleIncomingWebhook: error marshaling response: %s", err)
		res.WriteHeader(500)
		return
	}

	res.Header().Add("Content-type", "application/json")
	res.WriteHeader(200)
	res.Write(resBodyBytes)
}
