package handler

import (
	"bytes"
	"encoding/hex"
	"fmt"
	"net/http"

	"golang.org/x/crypto/nacl/sign"
)

func decodePublicKey(hexPubkey string) ([32]byte, error) {
	var pubkeyBytes [32]byte
	var err error
	_, err = hex.Decode(pubkeyBytes[:], []byte(hexPubkey))
	if err != nil {
		return pubkeyBytes, fmt.Errorf("failed to decode config pubkey: %w", err)
	}
	return pubkeyBytes, nil
}

func (h *Handler) validateWebhookSignature(reqHeaders http.Header, reqBody []byte) bool {
	hexSignature := reqHeaders.Get("X-Signature-Ed25519")
	signature, err := hex.DecodeString(hexSignature)
	if err != nil {
		return false
	}
	timestamp := reqHeaders.Get("X-Signature-Timestamp")
	var signPayload bytes.Buffer
	signPayload.Write(signature)
	signPayload.WriteString(timestamp)
	signPayload.Write(reqBody)
	_, valid := sign.Open(nil, signPayload.Bytes(), &h.pubkey)
	return valid
}
