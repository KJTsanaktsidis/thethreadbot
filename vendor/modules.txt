# github.com/cespare/xxhash v1.1.0
github.com/cespare/xxhash
# github.com/cespare/xxhash/v2 v2.1.1
github.com/cespare/xxhash/v2
# github.com/dgraph-io/badger/v3 v3.2103.1
## explicit
github.com/dgraph-io/badger/v3
github.com/dgraph-io/badger/v3/fb
github.com/dgraph-io/badger/v3/options
github.com/dgraph-io/badger/v3/pb
github.com/dgraph-io/badger/v3/skl
github.com/dgraph-io/badger/v3/table
github.com/dgraph-io/badger/v3/trie
github.com/dgraph-io/badger/v3/y
# github.com/dgraph-io/ristretto v0.1.0
github.com/dgraph-io/ristretto
github.com/dgraph-io/ristretto/z
github.com/dgraph-io/ristretto/z/simd
# github.com/dustin/go-humanize v1.0.0
github.com/dustin/go-humanize
# github.com/fsnotify/fsnotify v1.4.9
github.com/fsnotify/fsnotify
# github.com/gogo/protobuf v1.3.2
github.com/gogo/protobuf/proto
# github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
github.com/golang/glog
# github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e
github.com/golang/groupcache/lru
# github.com/golang/protobuf v1.5.2
github.com/golang/protobuf/proto
# github.com/golang/snappy v0.0.3
github.com/golang/snappy
# github.com/google/flatbuffers v1.12.0
github.com/google/flatbuffers/go
# github.com/gorilla/mux v1.8.0
## explicit
github.com/gorilla/mux
# github.com/hashicorp/hcl v1.0.0
github.com/hashicorp/hcl
github.com/hashicorp/hcl/hcl/ast
github.com/hashicorp/hcl/hcl/parser
github.com/hashicorp/hcl/hcl/printer
github.com/hashicorp/hcl/hcl/scanner
github.com/hashicorp/hcl/hcl/strconv
github.com/hashicorp/hcl/hcl/token
github.com/hashicorp/hcl/json/parser
github.com/hashicorp/hcl/json/scanner
github.com/hashicorp/hcl/json/token
# github.com/inconshreveable/mousetrap v1.0.0
github.com/inconshreveable/mousetrap
# github.com/klauspost/compress v1.12.3
github.com/klauspost/compress/fse
github.com/klauspost/compress/huff0
github.com/klauspost/compress/zstd
github.com/klauspost/compress/zstd/internal/xxhash
# github.com/magiconair/properties v1.8.5
github.com/magiconair/properties
# github.com/mitchellh/mapstructure v1.4.1
## explicit
github.com/mitchellh/mapstructure
# github.com/pelletier/go-toml v1.9.3
github.com/pelletier/go-toml
# github.com/pkg/errors v0.9.1
github.com/pkg/errors
# github.com/sirupsen/logrus v1.8.1
## explicit
github.com/sirupsen/logrus
# github.com/spf13/afero v1.6.0
github.com/spf13/afero
github.com/spf13/afero/mem
# github.com/spf13/cast v1.4.0
## explicit
github.com/spf13/cast
# github.com/spf13/cobra v1.2.1
## explicit
github.com/spf13/cobra
# github.com/spf13/jwalterweatherman v1.1.0
github.com/spf13/jwalterweatherman
# github.com/spf13/pflag v1.0.5
github.com/spf13/pflag
# github.com/spf13/viper v1.8.1
## explicit
github.com/spf13/viper
# github.com/subosito/gotenv v1.2.0
github.com/subosito/gotenv
# github.com/thanhpk/randstr v1.0.4
## explicit
github.com/thanhpk/randstr
# go.opencensus.io v0.23.0
go.opencensus.io
go.opencensus.io/internal
go.opencensus.io/trace
go.opencensus.io/trace/internal
go.opencensus.io/trace/tracestate
# golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97
## explicit
golang.org/x/crypto/ed25519
golang.org/x/crypto/ed25519/internal/edwards25519
golang.org/x/crypto/internal/subtle
golang.org/x/crypto/nacl/sign
# golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4
golang.org/x/net/internal/timeseries
golang.org/x/net/trace
# golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
## explicit
golang.org/x/sync/errgroup
# golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c
## explicit
golang.org/x/sys/internal/unsafeheader
golang.org/x/sys/unix
golang.org/x/sys/windows
# golang.org/x/text v0.3.6
## explicit
golang.org/x/text/transform
golang.org/x/text/unicode/norm
# google.golang.org/protobuf v1.26.0
google.golang.org/protobuf/encoding/prototext
google.golang.org/protobuf/encoding/protowire
google.golang.org/protobuf/internal/descfmt
google.golang.org/protobuf/internal/descopts
google.golang.org/protobuf/internal/detrand
google.golang.org/protobuf/internal/encoding/defval
google.golang.org/protobuf/internal/encoding/messageset
google.golang.org/protobuf/internal/encoding/tag
google.golang.org/protobuf/internal/encoding/text
google.golang.org/protobuf/internal/errors
google.golang.org/protobuf/internal/filedesc
google.golang.org/protobuf/internal/filetype
google.golang.org/protobuf/internal/flags
google.golang.org/protobuf/internal/genid
google.golang.org/protobuf/internal/impl
google.golang.org/protobuf/internal/order
google.golang.org/protobuf/internal/pragma
google.golang.org/protobuf/internal/set
google.golang.org/protobuf/internal/strs
google.golang.org/protobuf/internal/version
google.golang.org/protobuf/proto
google.golang.org/protobuf/reflect/protodesc
google.golang.org/protobuf/reflect/protoreflect
google.golang.org/protobuf/reflect/protoregistry
google.golang.org/protobuf/runtime/protoiface
google.golang.org/protobuf/runtime/protoimpl
google.golang.org/protobuf/types/descriptorpb
# gopkg.in/ini.v1 v1.62.0
gopkg.in/ini.v1
# gopkg.in/yaml.v2 v2.4.0
gopkg.in/yaml.v2
