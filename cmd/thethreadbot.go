package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"syscall"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"golang.org/x/sync/errgroup"

	"gitlab.com/KJTsanaktsidis/thethreadbot/internal/config"
	"gitlab.com/KJTsanaktsidis/thethreadbot/internal/handler"
	"gitlab.com/KJTsanaktsidis/thethreadbot/internal/scheduler"
	"gitlab.com/KJTsanaktsidis/thethreadbot/internal/slashregister"
)

func main() {
	rootCmd := &cobra.Command{
		Use:   "thethreadbot",
		Short: "TheThreadBot is a Discord bot to keep threads alive by pinging them every 24 hours",
		RunE:  run,
	}
	if err := rootCmd.Execute(); err != nil {
		log.Fatalf("thethreadbot: fatal error: %s", err.Error())
		os.Exit(1)
	}
}

func run(cmd *cobra.Command, args []string) error {
	// Set up execution environment.
	c := config.Config{}
	httpClient := http.DefaultClient
	rootCtx, rootCtxCancel := context.WithCancel(context.Background())
	defer rootCtxCancel()
	ctx, cancel := signal.NotifyContext(rootCtx, syscall.SIGINT, syscall.SIGTERM)
	defer cancel()
	logger := logrus.New()
	logger.SetOutput(os.Stdout)
	logger.SetLevel(logrus.DebugLevel)

	// Load up config
	viper.SetConfigName("thethreadbot")
	viper.SetConfigType("toml")
	viper.AddConfigPath(".")
	viper.AddConfigPath("/etc/")
	viper.SetDefault("DiscordAPIEndpoint", "https://discord.com/api/v9")
	viper.SetDefault("HttpListenAddr", "0.0.0.0")
	viper.SetDefault("HttpListenPort", 8196)
	if err := viper.ReadInConfig(); err != nil {
		return fmt.Errorf("failed to read viper config: %w", err)
	}
	if err := viper.Unmarshal(&c); err != nil {
		return fmt.Errorf("failed to unmarshal viper config: %w", err)
	}

	// Let the user know how to install the bot.
	linkToInstallParams := url.Values{}
	linkToInstallParams.Add("client_id", c.OAuth2ClientID)
	linkToInstallParams.Add("scope", "bot applications.commands")
	linkToInstallParams.Add("permissions", "34359740416") // Send Messages & Public Threads
	linkToInstall := fmt.Sprintf("%s/oauth2/authorize?%s", c.DiscordAPIEndpoint, linkToInstallParams.Encode())
	logger.Infof("Install the app by visiting %s", linkToInstall)
	logger.Infof("Will ping every %s", c.PingInterval.String())

	// Prepare slash commands
	if err := slashregister.RegisterSlashCommands(ctx, c, httpClient, logger); err != nil {
		return fmt.Errorf("failed registering slash-commands at startup: %w", err)
	}

	sched, err := scheduler.NewScheduler(ctx, c, httpClient, logger)
	if err != nil {
		return fmt.Errorf("failed to make scheduler: %w", err)
	}

	eg, egCtx := errgroup.WithContext(ctx)
	eg.Go(func() error {
		defer sched.Close()
		return sched.Run(egCtx)
	})
	eg.Go(func() error {
		if err := handler.RunHTTPServer(egCtx, c, logger, sched); err != nil {
			return fmt.Errorf("failed starting http server: %w", err)
		}
		return nil
	})

	return eg.Wait()
}
